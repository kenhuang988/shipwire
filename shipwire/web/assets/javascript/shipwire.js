var page = function () {};
page.prototype = {
     fields: { 
         "product": [
            {name: "name",  label:"Name"},
            {name: "description",  label:"Description"},
            {name: "width",  label:"Width"},
            {name: "height", label:"Height"},
            {name: "value",  label:"Value (in US Dollars)"}
        ],
        "order": [
            {name: "recipient",  label:"Recipient Name"},
            {name: "address",  label:"Street Address"},
            {name: "city",  label:"City"},
            {name: "state",  label:"State"},
            {name: "zip",  label:"Zip Code"},
            {name: "phone",  label:"Phone Number"},
            {name: "product",  label:"Product"},
            {name: "quantity",  label:"Quantity"}
        ]
     },
     init : function () {
        var self = this;
        $.subscribe("get/product", self.loadData, self);
        $.subscribe("get/order", self.loadData, self);
        $.subscribe("set/product", self.loadResult, self);
        $.subscribe("set/order", self.loadResult, self);
        $.subscribe("delete/product", self.loadResult, self);
        $.subscribe("delete/order", self.loadResult, self);
        
        var page = $('#type').val();
        if (page !=undefined){
            $(".sidebar ul li#" + page).addClass("active");
        }    
        $('.configForm').submit(function() {self.save_form(); return false;});
        $('#new').on('click',function(){
            window.location=$('#baseurl').val() + 'index.php/shipwire/page/'+$('#type').val()+'/new';
        });
        $('.data-table').on('click', '.edit', function(){
            window.location=$('#baseurl').val() + 'index.php/shipwire/page/'+$('#type').val()+'/edit/'+$(this).attr('data_id');
        });
        $('.data-table').on('click', '.delete', function(){
            var id =1;
            self.get_request("delete/"+$('#type').val(), $(this).attr('data_id'));
        });
        $('.entryTable').on('change', 'input[type="text"], select, textarea',function(){
            $('input[value="Save"]').removeClass("disabled");
            $(this).addClass("change");
        });
        
        if ($('.entryTable').length>0){
            self.loadForm();
            if ($('#type').val()=="order"){
                self.get_request("get/product",'','',false);
            }
        } 
        
        if ($('#id').val()!="0" || $('.data-table').length!=0){
            self.get_request("get/"+$('#type').val(), $('#id').val());
        }
        
    },
    save_form: function(){
        var self = this;
        if($('input[value="Save"]:visible').hasClass("disabled")){
            return false;
        }

        var validator = $(this).validate({
            ignore: '.novalidate',
            errorPlacement: function (error, element) {
                    if (element.hasClass('errors-above'))
                        error.appendTo(element.prevAll(".error-container").first());
                    else
                        error.insertAfter(element);
            }   
        });

        if (validator.form()){
            var send_data={};
            $('input[type="text"], input[type="password"], select, textarea').each(function(){
                if ((!this.disabled) && $(this).hasClass("change")) send_data[$(this).attr("name")]=$(this).val();
            });
            $('.change').removeClass("change");
            $('input[value="Save"]').addClass("disabled");
            self.get_request("set/"+$('#type').val(), $('#id').val(), send_data);
        } else {
            return false;
        }
        return false;
    },
    get_request:function(action, url_param, send_data, async)
    {
        try {
            if (async==undefined) async=true;
            var send_str = (typeof send_data !== 'undefined')?JSON.stringify(send_data):"";
            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: $('#baseurl').val()+'index.php/shipwire/api/' + action + '/' + url_param,
                cache: false,
                async: async,
                data: send_str,
                dataType : "json",
                success: function(data) {
                    $.publish(action, data);
                }
            });
        } catch(e) {

            }

    },
    loadForm: function(){
        var fields=this.fields[$('#type').val()];
        $('.entryTable').html('');
        if (fields){
            $.each(fields, function(index, field){
                if(field.name=="product"){
                    $('.entryTable').append('<div class="row"><div class="label">'+field.label+':</div><div class="input"><select name="'+field.name+'" id="'+field.name+'"></select></div></div>');
                } else {
                    $('.entryTable').append('<div class="row"><div class="label">'+field.label+':</div><div class="input"><input type="text" name="'+field.name+'" id="'+field.name+'" value=""></div></div>');  
                }
            })
        }
    },
    loadData: function(resp_data){
        if ($('.entryTable').length>0){
             this.loadFormData(resp_data);
        } else {
            this.loadTableData(resp_data);
        }
        
    },
    loadTableData: function(resp_data){
        var fields=this.fields[$('#type').val()];
        var type=$('#type').val();
        $('.data-table thead').html('<tr></tr>');
        $('.data-table tbody').html('');
        if (fields){
            $.each(fields, function(index, field){
                $('.data-table thead tr').append('<th>' + field.label + '</th>');
            });
            $('.data-table thead tr').append('<th></th>');
            $.each(resp_data, function(index, data){
                var id=data[type+'_id'];
                var rowhtml='';
                $.each(fields, function(index, field){
                    var key=field.name;
                    if (key=="product") key+="_name"
                    var value=data[key]?data[key]:"";
                    rowhtml += '<td>'+value+'</td>';
                });
                $('.data-table tbody').append('<tr>' + rowhtml + '<td><a class="edit" data_id="'+id+'">edit</a><a class="delete" data_id="'+id+'">delete</a></td></tr>');
            });
        }
        
    },
    loadFormData: function(resp_data){
        if (resp_data.length>0) {
            if($('#type').val()=="order" && resp_data[0].product_id){
                $("select#product").html(''); 
                $.each(resp_data, function(index, product){
                   $("select#product").append('<option value="'+product.product_id+'">'+product.name+'</option>'); 
                });
            } else {
                $.each(resp_data[0], function(key, value){
                   $("input#"+key+",select#"+key).val(value); 
                });
            }
        }
         
    },
    loadResult: function(resp_data){       
        window.location=$('#baseurl').val() + 'index.php/shipwire/page/'+$('#type').val();
    }
    
};


$(document).ready(function () {
    var obj = new page();
    obj.init();
});


