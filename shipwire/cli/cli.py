#!/usr/bin/python
from optparse import OptionParser
import sys, getopt, os
import collections
import json
import requests
import warnings


BASE_API_URL = "http://localhost/shipwire/index.php/shipwire/api/"

class ConfigureAPI:
    
    def parseProductCSV(self, filename):
        if os.path.isfile(filename):
            fields=None
            url = BASE_API_URL + "set/product"
            with open(filename) as fp:
                for line in fp:
                    line=line.strip()
                    if fields is None:
                        fields=line.split(',')
                    else:
                        values=line.split(',')
                        data = dict(zip(fields, values))
                        r = requests.post(url, data=json.dumps(data))
        else:
            print "No such file: " + filename

    def configure(self, options):
        self.options=options
        url = BASE_API_URL + options.action + "/" + options.type + "/" + str(options.id)
        data={};
        if (options.data is not None):
            pairs=options.data.split(',')
            for p in pairs:
                pair=p.split("=")
                if (len(pair)==2):
                    data[pair[0]]=pair[1];
        if(options.action=="set" and len(data)==0):
            print "No data to set"
            return;
        r = requests.post(url, data=json.dumps(data))
        try:
            if options.action=="get" and options.id>0:
                self.displaySingle(json.loads(r.text, object_pairs_hook=collections.OrderedDict))
            elif options.action=="get" and options.type=="product":
                self.displayProductTable(json.loads(r.text, object_pairs_hook=collections.OrderedDict))
            elif options.action=="get" and options.type=="order":
                self.displayOrderTable(json.loads(r.text, object_pairs_hook=collections.OrderedDict))
            else:
                self.displayResult(json.loads(r.text, object_pairs_hook=collections.OrderedDict))
        except ValueError, e:
            print r.text
                
        else:
            return {"status":1, "message": "Invalid command"}  
    
    def printOutput(self, data, rjust=[], indent="\t"):
        try:
            sys.stdout.write("\n")
            col_width=[]
            for c, row in enumerate(data):
                for i, word in enumerate(row):
                    if c==0:
                        col_width.append(len(word))
                    elif col_width[i]<len(word):
                        col_width[i]=len(word)

            for row in data:
                sys.stdout.write(indent)
                for i, word in enumerate(row):
                    if i in rjust:
                        sys.stdout.write(word.rjust(col_width[i]+2))
                    else:
                        sys.stdout.write(word.ljust(col_width[i]+2))
                sys.stdout.write("\n")
            sys.stdout.write("\n")
        except IOError as e:
            pass

    def displayResult(self, ret_data):
        if (ret_data["status"] != "0"):
            print ret_data["message"];

    def displaySingle(self, ret_data):
        print "single"
        print ret_data;
        if len(ret_data)==0:
            print "Not found"
        else:
            data=[]
            for key, item in ret_data[0].iteritems():
               data.append([key+":", item])
            self.printOutput(data)

    def displayProductTable(self, ret_data):
        displayOrder=["product_id", "name", "description", "width", "height", "value"]
        data=[]
        data.append(["ID", "Name", "Description", "Width", "Height", "Value"])
        data.append(["--", "----", "-----------", "-----", "------", "-----"])
        for item in ret_data:
            row=[]
            for k in displayOrder:
                if (item[k] is None):
                    row.append("---")
                else:
                    row.append(str(item[k]))
            data.append(row)
        self.printOutput(data, [], "")

    def displayOrderTable(self, ret_data):
        displayOrder=["order_id", "recipient", "address", "city", "state", "zip", "phone", "product_name", "quantity"]
        data=[]
        data.append(["ID", "recipient", "address", "city", "state", "zip_code", "phone", "product", "quantity"])
        data.append(["--", "---------", "-------", "----", "-----", "--------", "-----", "-------", "--------"])
        for item in ret_data:
            row=[]
            for k in displayOrder:
                if (item[k] is None):
                    row.append("---")
                else:
                    row.append(str(item[k]))
            data.append(row)
        self.printOutput(data, [], "")
   
def main(args):
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="csv_file",
        help="read from product CSV FILE", metavar="FILE")
    parser.add_option("-a", "--action", dest="action", type='choice',
        choices=['get', 'set', 'delete',], 
        default='get', help="action to perform")
    parser.add_option("-t", "--type", dest="type", type='choice',
        choices=['product', 'order',],
        default='product', help="category type")
    parser.add_option("-i", "--id", dest="id", default="0", type="int",
        help="category type id")
    parser.add_option("-d", "--data", dest="data", 
        help="data to update")
    (options, args) = parser.parse_args()
    confObj = ConfigureAPI()
    if options.csv_file is None:
        confObj.configure(options)
    else:
        confObj.parseProductCSV(options.csv_file)

if __name__ == "__main__":
    main(sys.argv[1:])