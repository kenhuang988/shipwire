<div class="content table">
    <div class="page-header">
        <h2><?php echo ucfirst($type);?></h2>
        <div class="buttons">
            <input type="button" id="new" value="New" class="button">
        </div>
        <div id="page_message" class="page-msg"></div>
    </div>
    <div class="page-content">
    <table class="data-table selectable">
    <thead>
    </thead>
    <tbody>
    </tbody>
</table>
</div>
</div>