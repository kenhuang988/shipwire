<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipwire_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function get_product($id){
        if ($id>0){
            $query = $this->db->get_where('product', array('product_id' => $id));
        } else {
            $query = $this->db->get('product');
        }
        return $query->result();
        
    }
    
    public function set_product($id, $data){
        if ($id>0){
            $this->db->where('product_id', $id);
            $this->db->update('product', $data); 
        } else {
            $this->db->insert('product', $data); 
        }
        $result["status"]="0";
        $result["message"]="Set product successfully";
        return $result;
    }
    
    public function delete_product($id){
        $this->db->delete('product', array('product_id' => $id)); 
        $result["status"]="0";
        $result["message"]="Delete product successfully";
        return $result;
    }
    
    public function get_order($id){
        if ($id>0){
            $query = $this->db->get_where('order', array('order_id' => $id));
        } else {
            $query = $this->db->query('SELECT `order`.*, product.name AS product_name FROM `order` LEFT JOIN product ON `order`.product=product.product_id');
        }
        return $query->result();
        
    }
    
    public function set_order($id, $data){
        if ($id>0){
            $this->db->where('order_id', $id);
            $this->db->update('order', $data); 
        } else {
            $this->db->insert('order', $data); 
        }
        $result["status"]="0";
        $result["message"]="Set order successfully";
        return $result;
    }
    
    public function delete_order($id){
        $this->db->delete('order', array('order_id' => $id)); 
        $result["status"]="0";
        $result["message"]="Delete order successfully";
        return $result;
    }
}
?>