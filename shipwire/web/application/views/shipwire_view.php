<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Shipwire</title>
<link type="text/css" href="<?php echo base_url();?>assets/jquery-ui-1.11.0/jquery-ui.css" rel="stylesheet">
<link type="text/css" href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!--[if IE 9]>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ie9.css">
<![endif]-->
<script type="text/javascript" src="<?php echo base_url();?>assets/javascript/lib/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-ui-1.11.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/javascript/lib/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/javascript/lib/additional-methods.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/javascript/lib/jquery_subscribe.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/javascript/shipwire.js"></script>
</head>
<body>
<div class="banner">
</div>
<div class="main">
<div class="sidebar menu">
<ul>
<li id="product"><a class="menu_item" name="product" href="<?php echo base_url();?>index.php/shipwire/page/product">Product</a> </li>
<li id="order"><a class="menu_item" name="order" href="<?php echo base_url();?>index.php/shipwire/page/order">Order</a> </li>
</ul>
</div>

<form class="configForm">
<input type="hidden" id="baseurl" name="action" value="<?php echo base_url();?>"/>
<input type="hidden" id="action" name="action" value="<?php echo $action;?>"/>
<input type="hidden" id="type" name="type" value="<?php echo $type;?>"/>
<input type="hidden" id="id" name="id" value="<?php echo $id;?>"/>
<?php require_once($content.".php");?>
</form>
</div>
</body></html>