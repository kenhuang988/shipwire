# README #

1. Requirment:
    php 5
    mysql
    python 2.7

   Frameworks are used:
    CodeIgniter 2.2
    JQuery

2. unzip shipwire.tar.gz

3. import database schema
   > mysql -u username -p database_name < .../shipwire/dbscript/shipwire.sql

4. create a link to shipwire/web in DocumentRoot
   > ln -s  .../shipwire/web shipwire

5. configure database
   open .../shipwire/web/application/config/database.php
   change the following to correct setting:
   
    $db['default']['hostname'] = 'localhost';
    $db['default']['username'] = 'root';
    $db['default']['password'] = 'admin123';
    $db['default']['database'] = 'shipwire'

6. If you like to browse from remote machine, change base url to correct host name.
   open .../shipwire/web/application/config/config.php

   Change the following line with correct hostname:
   
   $config['base_url']	= 'http://localhost/shipwire/';
   
7. browse url
    http://localhost/shipwire  or http://hostname/shipwire


CLI usage:
1.  Add new product

> ./cli.py -t product -a set -d name=apple,description=red,width=1,height=1,value=2.00

2.  Display product all products
    ./cli.py -t product

    Display single product
    ./cli.py -t product --id 1   

3. Update product
   ./cli.py -t product -a set --id 1 -d name=apple,description=red,width=1,height=1,value=2.00


4.  Process CSV file
    ./cli.py -f product.csv




   