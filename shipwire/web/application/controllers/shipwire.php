<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipwire extends CI_Controller {

    function __construct()
    {
      parent::__construct();
    }
    
    public function index(){
        $this->page('product'); 
    }
    
    public function page($type, $action='', $id=0){
        $data=array();
        $data["action"]= $action;
        $data["type"]= $type;
        $data["id"]=$id;
        $data["content"]="table";
        if ($action=="edit" || $action=="new"){
            $data["content"]="form";
        }
        $this->load->view('shipwire_view', $data);
    }
    
    public function api($action, $type, $id=0){ 
        header('Content-type: application/json');
        $this->load->model('shipwire_model');
        $result=array();
        $result["status"]="1";
        $result["message"]="api not found";  
        if ($action=="get"){
            switch ($type) {
                case "product":
                    $result=$this->shipwire_model->get_product($id);
                    break;
                case "order":
                    $result=$this->shipwire_model->get_order($id);
                    break;
            }
        } else if ($action=="set"){
            $input_data = json_decode(trim(file_get_contents('php://input')), true);
            switch ($type) {
                case "product":
                    $result=$this->shipwire_model->set_product($id, $input_data);
                    break;
                case "order":
                    $result=$this->shipwire_model->set_order($id, $input_data);
                    break;
            }
        } else if ($action=="delete"){
            switch ($type) {
                case "product":
                    $result=$this->shipwire_model->delete_product($id);
                    break;
                case "order":
                    $result=$this->shipwire_model->delete_order($id);
                    break;
            }
        }
        echo json_encode($result);
    }
   
}

